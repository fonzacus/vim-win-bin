[ORIGINAL](https://github.com/micbou/vim-for-windows)

[![AppVeyor build status](https://ci.appveyor.com/api/projects/status/twwsyen7192tjq17/branch/master?svg=true)](https://ci.appveyor.com/project/micbou/vim-for-windows/branch/master)

# Vim for Windows

Vim 32-bit and 64-bit releases for Windows including support for the following
interfaces:
 - [Lua 5.3](https://sourceforge.net/projects/luabinaries/files/);
 - [Perl 5.26](https://downloads.activestate.com/ActivePerl/releases);
 - [Python 2.7](https://www.python.org/downloads/release/python-2715/);
 - [Python 3.7](https://www.python.org/downloads/release/python-370/);
 - [Racket 7.0](https://racket-lang.org/download/);
 - [Ruby 2.5](https://rubyinstaller.org/downloads/);
 - [Tcl 8.6](https://downloads.activestate.com/ActiveTcl/releases).

Terminal support is also available (see `:h terminal`).

## Why? There is already nightly official Vim binaries for Windows.

For multiple reasons:
 - 64-bit installer;
 - latest versions of the interfaces;
 - releases available on
